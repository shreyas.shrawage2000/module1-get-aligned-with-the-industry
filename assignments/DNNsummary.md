**What is DNN.**

Deep Learning is a subfield of machine learning concerned with algorithms inspired by the structure and function of the brain.
Deep Neural Networks is related to Deep learning.

Basically, an artificial neural network (ANN) with multiple layers between the input and output layers is a DNN.

![](https://orbograph.com/wp-content/uploads/2019/01/DeepLearn.png)

A simplified version of Deep Neural Network is represented as a hierarchical (layered) organization of neurons (similar to the neurons in the brain) with connections to other neurons. These neurons pass a message or signal to other neurons based on the received input and form a complex network that learns with some feedback mechanism. The following diagram represents an ’N’ layered Deep Neural Network.

As you can reference in the above figure, the input data is consumed by the neurons in the first layer (not hidden) which then provide an output to the neurons within next layer and so on which provides the final output. 

