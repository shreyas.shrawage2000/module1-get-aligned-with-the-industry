A **Convolutional Neural Network** (CNN) is a Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other.

A ConvNet is able to successfully capture the Spatial and Temporal dependencies in an image through the application of relevant filters.

![](https://miro.medium.com/max/500/1*15yDvGKV47a0nkf5qLKOOQ.png)

In the figure, we have an RGB image which has been separated by its three color planes — Red, Green, and Blue. There are a number of such color spaces in which images exist — Grayscale, RGB, HSV, CMYK, etc.

The role of the CNN is to reduce the images into a form which is easier to process, without losing features which are critical for getting a good prediction.

**The Kernel**

The objective of the Convolution Operation is to extract the high-level features such as edges, from the input image. ConvNets need not be limited to only one Convolutional Layer.

![](https://miro.medium.com/max/395/1*1VJDP6qDY9-ExTuQVEOlVg.gif)

Such operations result into two types, one in which the convolved feature is reduced in dimensionality as compared to the input, and the other in which the dimensionality is either increased or remains the same. This is done by applying Valid Padding in case of the former, or Same Padding in the case of the latter.

**Pooling Layer**

Similar to the Convolutional Layer, the Pooling layer is responsible for reducing the spatial size of the Convolved Feature.
This is to decrease the computational power required to process the data through dimensionality reduction.

![](https://miro.medium.com/max/500/1*KQIEqhxzICU7thjaQBfPBQ.png)

There are two types of Pooling: 
1. Max Pooling
2. Average Pooling.

**Max Pooling** returns the maximum value from the portion of the image covered by the Kernel.

Whereas, **Average Pooling** returns the average of all the values from the portion of the image covered by the Kernel.

